## Hi there 👋
### Mario Fernando Fernández Muñoz
## Resume: 
[https://resume.io/r/8EjQ1dtnJ](https://resume.io/r/8EjQ1dtnJ)
## Main technologies
Java, SpringBoot, Mulesoft, Python.

---
## _This is a Rest API to fetch mexican zip codes._
> SpringBoot, Oracle JDK 8, Swagger, Maven, JUnit, Mockito, Jacoco, MySQL & GCP

## Documentation and Release 
### Swagger API Documentation:
>Please refer to the swagger documentation in the following [link](https://zipcodes-313816.uc.r.appspot.com/swagger-ui.html).

### Release URL:
>[https://zipcodes-313816.uc.r.appspot.com/api/zip-codes/06140](https://zipcodes-313816.uc.r.appspot.com/api/zip-codes/06140)

## Installation
### Local Database:
> Setup a **MySQL** database and execute the sql [zipcodes.sql](src/main/resources/zipcodes.sql)

### Clone the project:
```sh
git clone https://mffm23@bitbucket.org/mabuho_fdz/zip-codes.git
git checkout master
git pull
```
### Update project properties:
Update the Database properties in [bootstrap.yml](src/main/resources/bootstrap.yml) under `### DEV ##` section:
```
username: <YOUR_DB_USERNAME>
password: <YOUR_DB_PASSWORD>
```
### Build project:
Install the dependencies and start the server:
```sh
mvn clean install -DskipTests
mvn spring-boot:run
```

### Test the API locally:
Once the service is up an running hit the following URL in Postman or in a web browser:
> `http://localhost:8080/api/zip-codes/{zipCode}`

Where `zipCode` is 5 digit string.

####  Example:
> `http://localhost:8080/api/zip-codes/06140`

StatusCode:  ```200 - Success```

Response body:
```yaml
{
    "zip_code": "06140",
    "locality": "CIUDAD DE MEXICO",
    "federal_entity": "CIUDAD DE MEXICO",
    "settlements": [
        {
            "name": "CONDESA",
            "zone_type": "Urbano",
            "settlement_type": "Colonia"
        }
    ]
}
```

#### Example: 
> `http://localhost:8080/api/zip-codes/14040`

StatusCode: ```200 - Success```

Response body:
```yaml
{
    "zip_code": "14040",
    "locality": "CIUDAD DE MEXICO",
    "federal_entity": "CIUDAD DE MEXICO",
    "settlements": [
        {
            "name": "CANTERA PUENTE DE PIEDRA",
            "zone_type": "Urbano",
            "settlement_type": "Colonia"
        },
        {
            "name": "PUEBLO QUIETO",
            "zone_type": "Urbano",
            "settlement_type": "Colonia"
        }
    ]
}
```
----

#### Example:  
> `http://localhost:8080/api/zip-codes/14040`

StatusCode: ```404 - Not Found```

Response body:
```yaml
{}
```
----

#### Example: 
> `http://localhost:8080/api/zip-codes/abcde` 

StatusCode: ```404 - Not Found```

Response body:
```yaml
{}
```
### Code coverage report:
This project runs JUnit tests under code coverage and creates a coverage report ```target/site/jacoco/index.html```. 

Run:
```
mvn clean verify
``` 

