/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50624
 Source Host           : localhost:3306
 Source Schema         : zipcodes

 Target Server Type    : MySQL
 Target Server Version : 50624
 File Encoding         : 65001

 Date: 16/05/2021 21:12:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for settlement
-- ----------------------------
DROP TABLE IF EXISTS `settlement`;
CREATE TABLE `settlement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `zone_type` varchar(255) DEFAULT NULL,
  `settlement_type` varchar(255) DEFAULT NULL,
  `zip_code_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_settlement_zip_code` (`zip_code_id`),
  CONSTRAINT `fk_settlement_zip_code` FOREIGN KEY (`zip_code_id`) REFERENCES `zip_code` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of settlement
-- ----------------------------
BEGIN;
INSERT INTO `settlement` VALUES (1, 'CONDESA', 'Urbano', 'Colonia', 1);
INSERT INTO `settlement` VALUES (2, 'CANTERA PUENTE DE PIEDRA', 'Urbano', 'Colonia', 2);
INSERT INTO `settlement` VALUES (3, 'PUEBLO QUIETO', 'Urbano', 'Colonia', 2);
COMMIT;

-- ----------------------------
-- Table structure for zip_code
-- ----------------------------
DROP TABLE IF EXISTS `zip_code`;
CREATE TABLE `zip_code` (
  `id` int(11) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `locality` varchar(255) NOT NULL,
  `federal_entity` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of zip_code
-- ----------------------------
BEGIN;
INSERT INTO `zip_code` VALUES (1, '06140', 'CIUDAD DE MEXICO', 'CIUDAD DE MEXICO');
INSERT INTO `zip_code` VALUES (2, '14040', 'CIUDAD DE MEXICO', 'CIUDAD DE MEXICO');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
