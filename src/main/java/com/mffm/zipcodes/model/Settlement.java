package com.mffm.zipcodes.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="settlement", schema="zipcodes")
public class Settlement implements Serializable {
	
	private static final long serialVersionUID = -3166861509331014350L;
	
	@Id
	@JsonIgnore
	private int id;
	@Column
	private String name;
	@Column
	private String zone_type;
	@Column
	private String settlement_type;
	
}
