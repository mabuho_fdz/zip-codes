package com.mffm.zipcodes.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="zip_code", schema="zipcodes")
public class ZipCode implements Serializable {

	private static final long serialVersionUID = -4806518600113497560L;
	
	@Column(name="id")
	@JsonIgnore
	private int zip_code_id;
	@Id
	private String zip_code;
	@Column
	private String locality;
	@Column
	private String federal_entity;
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true)
	@JoinColumn(name = "zip_code_id", referencedColumnName = "id")
	private List<Settlement> settlements;
	
}
