package com.mffm.zipcodes.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mffm.zipcodes.model.ZipCode;
import com.mffm.zipcodes.service.ZipCodeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api")
@Api(value="/api", protocols="http, https", produces="application/json")
@CrossOrigin
@Slf4j
@AllArgsConstructor
public class ZipCodeController {
	
	private final static String EMTPY_JSON = "{}";
	private static final String ZIP_CODE_PATTERN = "^[0-9]{5}$";  
	private final ZipCodeService zipCodeService;

	@GetMapping("/zip-codes/{zipCode}")
	@ApiImplicitParam(name="zipCode", defaultValue="00000")
	@ApiResponses(value={
			@ApiResponse(response=ZipCode.class, code = 200, message = "Success"), 
			@ApiResponse(response=Object.class, code = 404, message = "Not Found")
			})
	public ResponseEntity<Object> getZipCodeDetails(@PathVariable("zipCode") String zipCode) {
		if(zipCode == null|| !zipCode.matches(ZIP_CODE_PATTERN)) {
			log.info("Not valid ZipCode={}", zipCode);
			ResponseEntity.status(HttpStatus.NOT_FOUND).body(EMTPY_JSON);
		}
		log.debug("####### ZIP_CODE = {} #######", zipCode);
		
		Optional<ZipCode> zc = zipCodeService.getZipCodeDetails(zipCode);		
		
		return (!zc.isPresent()) ? 
				ResponseEntity.status(HttpStatus.NOT_FOUND).body(EMTPY_JSON) : 
				ResponseEntity.ok(zc.get());
	}
	
	@GetMapping("/zip-codes")
	@ApiIgnore
	public ResponseEntity<Object> getZipCodeDetails1() {
		return getZipCodeDetails("00000");
	}
	
	@GetMapping("/zip-codes/")
	@ApiIgnore
	public ResponseEntity<Object> getZipCodeDetails2() {
		return getZipCodeDetails("00000");
	}
	
}
