package com.mffm.zipcodes.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.mffm.zipcodes.model.ZipCode;
import com.mffm.zipcodes.repository.ZipCodeRepository;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class ZipCodeService implements IZipCodesService {
	
	final private ZipCodeRepository zipCodeRepository;

	@Override
	public Optional<ZipCode> getZipCodeDetails(String zip_code){
		Optional<ZipCode> resp = Optional.empty();
		try {
			resp = zipCodeRepository.findById(zip_code);
		} catch (Exception e) {
			log.error("EXCEPTION: {}", e);
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public List<ZipCode> getAll() {
		return zipCodeRepository.findAll();
	}
	
}
