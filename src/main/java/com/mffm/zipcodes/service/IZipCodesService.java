package com.mffm.zipcodes.service;

import java.util.List;
import java.util.Optional;

import com.mffm.zipcodes.model.ZipCode;

public interface IZipCodesService {

	Optional<ZipCode> getZipCodeDetails(String zc);
	List<ZipCode> getAll();
	
}
