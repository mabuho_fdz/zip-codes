package com.mffm.zipcodes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableJpaRepositories
//@EnableCaching
@Slf4j
public class ZipCodesApplication {

	public static void main(String[] args) {
		log.info(" #### ZipCodesApplication ####");
		SpringApplication.run(ZipCodesApplication.class, args);
	}

}
