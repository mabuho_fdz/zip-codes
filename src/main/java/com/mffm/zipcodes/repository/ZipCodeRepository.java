package com.mffm.zipcodes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mffm.zipcodes.model.ZipCode;

@Repository
public interface ZipCodeRepository extends JpaRepository<ZipCode, String> {
	
}
