package com.mffm.zipcodes.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mffm.zipcodes.model.Settlement;
import com.mffm.zipcodes.model.ZipCode;
import com.mffm.zipcodes.repository.ZipCodeRepository;

public class ZipCodeServiceTest {
	
	private static final String ZIP_CODE = "14040";
	private static final String INVALID_ZIP_CODE = "00000";
	
	private ZipCodeService zipCodeService;
	
	@Mock
	private ZipCodeRepository zipCodeRepository;
	
    private ZipCode zipCode;
    private Settlement settlement;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		zipCodeService = new ZipCodeService(zipCodeRepository);
		settlement = new Settlement(1, "Cantera Puente de Piedra", "Urbano", "Colonia");
		List<Settlement> settlements = new ArrayList<>();
		settlements.add(settlement);
		zipCode = new ZipCode(1, ZIP_CODE, "CIUDAD DE MEXICO", "CIUDAD DE MEXICO", settlements);
	}
	
	@Test
	public void test_get_zipcode_details() throws Exception {
		when(zipCodeRepository.findById(ZIP_CODE)).thenReturn(Optional.of(zipCode));
		Optional<ZipCode> zc = zipCodeService.getZipCodeDetails(ZIP_CODE);
		verify(zipCodeRepository, times(1)).findById(ZIP_CODE);
		assertNotNull("The object is null", zc);
		assertEquals("The expected object is not equal", zc, Optional.of(zipCode));
	}
	
	@Test(expected = Exception.class)
	public void test_get_zipcode_details_exception() throws Exception {
		when(zipCodeRepository.findById(INVALID_ZIP_CODE)).thenThrow(Exception.class);
		zipCodeService.getZipCodeDetails(ZIP_CODE);
		
	}

}
