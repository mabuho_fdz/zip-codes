package com.mffm.zipcodes.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.mffm.zipcodes.ZipCodesApplication;
import com.mffm.zipcodes.model.ZipCode;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=ZipCodesApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Profile("test")
@ActiveProfiles("test")
@Slf4j
public class ZipCodesControllerIT {

	private static final String ZIP_CODE_1 = "06140";
	private static final String ZIP_CODE_2 = "14040";
	private static final String INVALID_ZIP_CODE_1 = "00000";
	private static final String INVALID_ZIP_CODE_2 = "abcde";

	private static final String BASE_URL = "http://localhost";
	
	@LocalServerPort
	private String port;
	
	private static TestRestTemplate restTemplate;

	@Before
    public void init() {
		restTemplate = new TestRestTemplate();
	}
	
	@Test
	public void test_zip_code1_details_HTTPCodeOk200() {
		ResponseEntity<ZipCode> resp = restTemplate
				.getForEntity(getBaseURL(ZIP_CODE_1), ZipCode.class);
		log.debug("ResponseEntity={}", resp);
		assertNotNull("The response is null", resp);
		assertEquals("The status_code is not OK", resp.getStatusCode(), HttpStatus.OK);
		assertEquals("The size is nos 1", resp.getBody().getSettlements().size(), 1);
	}
	
	@Test
	public void test_zip_code2_details_HTTPCodeOk200() {
		ResponseEntity<ZipCode> resp = restTemplate
				.getForEntity(getBaseURL(ZIP_CODE_2), ZipCode.class);
		log.debug("ResponseEntity={}", resp);
		assertNotNull("The response is null", resp);
		assertEquals("The status_code is not OK", resp.getStatusCode(), HttpStatus.OK);
		assertEquals("The size is nos 2", resp.getBody().getSettlements().size(), 2);
	}
	
	@Test
	public void test_invalid_zip_code1_details_HTTPCodeNotFound404() {
		ResponseEntity<ZipCode> resp = restTemplate
				.getForEntity(getBaseURL(INVALID_ZIP_CODE_1), ZipCode.class);
		log.debug("ResponseEntity={}", resp.getBody().getSettlements());
		assertNotNull("The response is null", resp);
		assertEquals("The status_code is not NOT_FOUND", resp.getStatusCode(), HttpStatus.NOT_FOUND);
		assertNull("The object is not null", resp.getBody().getSettlements());
	}
	
	@Test
	public void test_invalid_zip_code2_details_HTTPCodeNotFound404() {
		ResponseEntity<ZipCode> resp = restTemplate
				.getForEntity(getBaseURL(INVALID_ZIP_CODE_2), ZipCode.class);
		log.debug("ResponseEntity={}", resp);
		assertNotNull("The response is null", resp);
		assertEquals("The status_code is not NOT_FOUND", resp.getStatusCode(), HttpStatus.NOT_FOUND);
		assertNull("The object is not null", resp.getBody().getSettlements());
	}
	
	private String getBaseURL(String arg){
		return BASE_URL.concat(":")
				.concat(port+ "")
				.concat("/api/zip-codes/")
				.concat(arg);
	}

}
