package com.mffm.zipcodes.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.mffm.zipcodes.model.ZipCode;
import com.mffm.zipcodes.service.ZipCodeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ZipCodeControllerTest {

	private static final String ZIP_CODE_1 = "06410";
	private static final String ZIP_CODE_2 = "14040";
	private static final String INVALID_ZIP_CODE_1 = "00000";
	private static final String INVALID_ZIP_CODE_2 = "abcde";
	private static final String ZIP_CODE_PATTERN = "^[0-9]{5}$";
	private static final String EMTPY_JSON = "{}"; 
	
	private ZipCodeController zipCodeController;

	@Mock
	private ZipCodeService zipCodeService;

	@Mock
	private ZipCode zipCode;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		zipCodeController = new ZipCodeController(zipCodeService);
	}

	@Test
	public void test_get_zip_code_details_with_valid_zipcode() throws Exception {
		assertThat((ZIP_CODE_1.matches(ZIP_CODE_PATTERN)));
		when(zipCodeService.getZipCodeDetails(ZIP_CODE_1)).thenReturn(Optional.of(zipCode));
		ResponseEntity<Object> resp = zipCodeController.getZipCodeDetails(ZIP_CODE_1);
		verify(zipCodeService, times(1)).getZipCodeDetails(ZIP_CODE_1);
		log.debug("Status_Code={} Reason={}", resp.getStatusCode().value(), resp.getStatusCode().getReasonPhrase());
		assertEquals("the status_code is not 200", resp.getStatusCode().value(), HttpStatus.OK.value());
		assertEquals("the status is not OK", resp.getStatusCode().getReasonPhrase(), HttpStatus.OK.getReasonPhrase());
		log.debug("Body={}", resp.getBody());
		assertNotEquals("The body is {}", resp.getBody(), EMTPY_JSON);
	}
	
	@Test
	public void test_zip_code_details_not_found_with_valid_zipcode(){
		assertThat((ZIP_CODE_2.matches(ZIP_CODE_PATTERN)));
		when(zipCodeService.getZipCodeDetails(ZIP_CODE_2)).thenReturn(Optional.empty());
		ResponseEntity<Object> resp = zipCodeController.getZipCodeDetails(ZIP_CODE_2);
		verify(zipCodeService, times(1)).getZipCodeDetails(ZIP_CODE_2);
		log.debug("Status_Code={} Reason={}", resp.getStatusCode().value(), resp.getStatusCode().getReasonPhrase());
		assertEquals("the status_code is not 404", resp.getStatusCode().value(), HttpStatus.NOT_FOUND.value());
		assertEquals("the status is not NOT_FOUND", resp.getStatusCode().getReasonPhrase(), HttpStatus.NOT_FOUND.getReasonPhrase());
		log.debug("Body={}", resp.getBody());
		assertEquals("The body is not {}", resp.getBody(), EMTPY_JSON);
	}
	
	@Test
	public void test_not_found_zip_code_details_with_invvalid_zipcode(){
		assertThat((INVALID_ZIP_CODE_1.matches(ZIP_CODE_PATTERN)));
		when(zipCodeService.getZipCodeDetails(INVALID_ZIP_CODE_1)).thenReturn(Optional.empty());
		ResponseEntity<Object> resp = zipCodeController.getZipCodeDetails(INVALID_ZIP_CODE_1);
		verify(zipCodeService, times(1)).getZipCodeDetails(INVALID_ZIP_CODE_1);
		log.debug("Status_Code={} Reason={}", resp.getStatusCode().value(), resp.getStatusCode().getReasonPhrase());
		assertEquals("the status_code is not 404", resp.getStatusCode().value(), HttpStatus.NOT_FOUND.value());
		assertEquals("the status is not NOT_FOUND", resp.getStatusCode().getReasonPhrase(), HttpStatus.NOT_FOUND.getReasonPhrase());
		log.debug("Body={}", resp.getBody());
		assertEquals("The body is not {}", resp.getBody(), EMTPY_JSON);
	}
	@Test
	public void test_invvalid_zipcode_not_match_numeric_pattern(){
		assertThat(!(INVALID_ZIP_CODE_2.matches(ZIP_CODE_PATTERN)));
		when(zipCodeService.getZipCodeDetails(INVALID_ZIP_CODE_2)).thenReturn(Optional.empty());
		ResponseEntity<Object> resp = zipCodeController.getZipCodeDetails(INVALID_ZIP_CODE_2);
		verify(zipCodeService, times(1)).getZipCodeDetails(INVALID_ZIP_CODE_2);
		log.debug("Status_Code={} Reason={}", resp.getStatusCode().value(), resp.getStatusCode().getReasonPhrase());
		assertEquals("the status_code is not 404", resp.getStatusCode().value(), HttpStatus.NOT_FOUND.value());
		assertEquals("the status is not NOT_FOUND", resp.getStatusCode().getReasonPhrase(), HttpStatus.NOT_FOUND.getReasonPhrase());
		log.debug("Body={}", resp.getBody());
		assertEquals("The body is not {}", resp.getBody(), EMTPY_JSON);
	}

}
