DELETE FROM zipcodes.settlement;
DELETE FROM zipcodes.zip_code;

INSERT INTO zipcodes.zip_code (id, zip_code, locality, federal_entity) VALUES (1, '06140', 'CIUDAD DE MEXICO', 'CIUDAD DE MEXICO');
INSERT INTO zipcodes.zip_code (id, zip_code, locality, federal_entity) VALUES (2, '14040', 'CIUDAD DE MEXICO', 'CIUDAD DE MEXICO');

INSERT INTO zipcodes.settlement (id, name, zone_type, settlement_type, zip_code_id) VALUES (1, 'CONDESA', 'Urbano', 'Colonia', 1);
INSERT INTO zipcodes.settlement (id, name, zone_type, settlement_type, zip_code_id) VALUES (2, 'CANTERA PUENTE DE PIEDRA', 'Urbano', 'Colonia', 2);
INSERT INTO zipcodes.settlement (id, name, zone_type, settlement_type, zip_code_id) VALUES (3, 'PUEBLOQUIETO', 'Urbano', 'Colonia', 2);